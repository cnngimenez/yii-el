Emacs commands for Yii

# Interactive (M-x) Commands

For initializing:

* `yii-init` specifies the base dir and the type of project.

For accessing files:

* `yii-model`
* `yii-controller`
* `yii-view`
* `yii-view-dir`

For executing Yii command:

* `yii-migrate-up`
* `yii-migrate-create`
* `yii-runserver`


