;;; yii.el --- 

;; Copyright 2018 Giménez, Christian
;;
;; Author: Giménez, Christian
;; Version: $Id: comandos-yii.el,v 0.0 2018/05/10 12:27:03 poo Exp $
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'comandos-yii)

;;; Code:

(provide 'comandos-yii)
(eval-when-compile
  (require 'cl))

(require 'dash)
(require 'ivy)

(defvar yii-base-dir nil
  "Yii project's base dir.")

(defvar yii-advanced nil
  "Is the current Yii project advanced or simple template?")

(defun yii--paths (type &optional model)
  "Return the path of model, controller, view or etc.

TYPE is a string like \"models\".
MODEL is a filename string like \"Person.php\" or \"\"."
  (if yii-advanced
      (list
       (concat yii-base-dir "/backend/" type "/" model)
       (concat yii-base-dir "/console/" type "/" model)
       (concat yii-base-dir "/frontend/" type "/" model))
    (list (concat yii-base-dir "/" type "/" model))
    ) ;; if
  ) ;; defun

(defun yii--files (type &optional relative)
  "Return all files that are in the model, view or controller of the
  project.

TYPE is a string like \"model\".
RELATIVE (Optional) If setted, show the relative path as the display string.
  Real data will still be absolute paths. "
  (let ((absolute-path (not relative))
	)
     (mapcar
      (lambda (abspath)
	"Return a (display . abspath).
display will be the filename according to an external `absolute-path' variable."
	(cons
	 (if relative
	     (file-name-nondirectory abspath)
	   (file-relative-name abspath yii-base-dir))
	 abspath)	
	) ;; lambda
      
    (-flatten      
     (mapcar
      (lambda (dir)
	"Return a list of files in the DIR."
	(when (file-exists-p dir)
	  (directory-files dir t "^[^\.].*")
	  )
	)
      (yii--paths type)))
     ) ;; mapcar
    ) ;; let
  ) ;; defun

(defun yii--is-advanced (directory)
  "Return true if the Yii project in directory follows the advanced template."
  (and (file-exists-p (concat directory "/backend"))
       (file-exists-p (concat directory "/frontend")))
  ) ;; defun

(defun yii-init (directory)
  "Search the base dir and initialize variables.

DIRECTORY must be a string with the yii path."
  (interactive
   (list 
    (read-directory-name "Yii base path? " nil nil t)
    ))

  (setq yii-base-dir directory)
  (setq yii-advanced (yii--is-advanced yii-base-dir))

  (message (concat "Yii configured as "
		   (propertize (if yii-advanced
				   "advanced" 
				 "base"
				 )
			       'face 'bold)
		   " project."))
  ) ;; defun

(defun yii--ask-file-open (lst-files)
  "Ask for a file to open."
  (ivy-read "Open File"
	    lst-files
	    :sort t
	    :action (lambda (x)
  		      (find-file (cdr x))
  		      )
	    )
  ;; (helm :sources (helm-build-sync-source "files"
  ;; 		   :candidates lst-files		     
  ;; 		   :fuzzy-match t
  ;; 		   :action (helm-make-actions
  ;; 			    "Open" 'find-file
  ;; 			    )
  ;; 		   )
  ;; 	:buffer "*Select File*"
  ;; 	)
  ) ;; defun

(defun yii--ask-file (lst-files)
  "Ask for a file to select."
  (let ((selected nil))
    (ivy-read "Select File"
	      lst-files
	      :sort t
	      :action (lambda (x)
			(setq selected x)
			)
	      )
    (cdr selected)
    ) ;; let
  ;; (helm :sources (helm-build-sync-source "files"
  ;; 		   :candidates lst-files		     
  ;; 		   :fuzzy-match t
  ;; 		   )
  ;; 	:buffer "*Select File*"
  ;; 	)
  ) ;; defun


					; __________________________________________________
					; Yii find file functions.

(defun yii-model ()
  "Open a model file."
  (interactive)
  ;; Ask to the user which model.
  (yii--ask-file-open (yii--files "models"))
  ) ;; defun

(defun yii-controller ()
  "Open a controller file"
  (interactive)
  (yii--ask-file-open (yii--files "controllers"))
  ) ;; defun

(defun yii-view ()
  "Open a view file."
  (interactive)
  (let* ((path (yii--ask-file (yii--files "views")))
	 (subpaths (mapcar
		    (lambda (filename)
		      (cons filename
			    (concat path "/" filename)))
		    (directory-files path)))
	 )
    (yii--ask-file-open subpaths)
    ) ;; let
  ) ;; defun

(defun yii-view-dir ()
  "Open a view directory."
  (interactive)
  (yii--ask-file-open (yii--files "views"))
  ) ;; defun

(defun yii-migration ()
  "Open a migration file"
  (interactive)
  (yii--ask-file-open (yii--files "migrations" t))
  ) ;; defun

					; __________________________________________________
					; Yii script commands.

;; TODO `yii-migrate-create'.
(defun yii-migrate-create (name)
  "Call migrate/create command."
  (interactive "MName?")
  ;; (let ((default-directory yii-base-dir))
  (eshell)
  (with-current-buffer eshell-buffer-name
    (eshell-bol) 
    (delete-region (point) (point-max))
    (insert "cd " yii-base-dir)
    (eshell-send-input)
    (insert "php yii migrate/create " name)
    (eshell-send-input)
    
    (goto-char (point-max))
    (switch-to-buffer (current-buffer))
    )
  ;; (call-process "php"
  ;; nil "*yii-migrate-create*" t
  ;; "yii" "migrate/create" name)
  ;; ) ;; let
  ) ;; defun

;; TODO `yii-migrate-up'.
(defun yii-migrate-up ()
  "Call migrate/up command."
  (interactive)
  (message "TODO")
  ) ;; defun

;; TODO `yii-runserver'.
(defun yii-runserver ()
  "Call runserver command."
  (interactive)
  (message "TODO")
  ) ;; defun

					; ____________________________________
					; Global Keys

(global-set-key "\C-cyi" 'yii-init)
(global-set-key "\C-cym" 'yii-model)
(global-set-key "\C-cyv" 'yii-view)
(global-set-key "\C-cyc" 'yii-controller)
(global-set-key "\C-cyM" 'yii-migration)


;;; comandos-yii.el ends here
